"""
# SimpleServer.py
# A basic web server
#
# Author: Richard Harrington
# Date Created: 10/21/2013
"""

import http.server as ht
import socketserver

class simple_server:

    def __init__(self):
        PORT=80
        Handler=ht.SimpleHTTPRequestHandler
        httpd=socketserver.TCPServer(("",PORT),Handler)
        print("serving at port",PORT)
        httpd.serve_forever()

ss=simple_server()
